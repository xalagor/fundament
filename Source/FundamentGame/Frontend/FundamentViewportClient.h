// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CommonGameViewportClient.h"
#include "UObject/UObjectGlobals.h"

#include "FundamentViewportClient.generated.h"

class UGameInstance;
class UObject;

UCLASS(BlueprintType)
class UFundamentViewportClient : public UCommonGameViewportClient
{
	GENERATED_BODY()

public:
	UFundamentViewportClient();

	virtual void Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice = true) override;
};
