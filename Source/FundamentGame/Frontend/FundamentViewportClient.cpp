// Copyright Epic Games, Inc. All Rights Reserved.

#include "FundamentViewportClient.h"

#include "CommonUISettings.h"
#include "ICommonUIModule.h"
#include "UObject/NameTypes.h"

class UGameInstance;

UFundamentViewportClient::UFundamentViewportClient()
	: Super(FObjectInitializer::Get())
{
}

void UFundamentViewportClient::Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice)
{
	Super::Init(WorldContext, OwningGameInstance, bCreateNewAudioDevice);
}
