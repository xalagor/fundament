// Copyright Denis Xalagor Petrov. MIT Licensed.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FundamentGameMode.generated.h"

/**
 *
 */
UCLASS()
class FUNDAMENTGAME_API AFundamentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

};
