// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"
#include "FundamentWorldSettings.generated.h"

/**
 * The default world settings object, used primarily to set the default gameplay experience to use when playing on this map
 * This class is used by setting 'WorldSettingsClass' in DefaultEngine.ini.
 */
UCLASS()
class FUNDAMENTGAME_API AFundamentWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:

	AFundamentWorldSettings(const FObjectInitializer& ObjectInitializer);
};
