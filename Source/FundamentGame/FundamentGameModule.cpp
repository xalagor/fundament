// Copyright Denis Xalagor Petrov. MIT Licensed.

#include "Modules/ModuleManager.h"

/**
 * FFundamentGameModule
 */
class FFundamentGameModule : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override
	{
	}

	virtual void ShutdownModule() override
	{
	}
};

IMPLEMENT_PRIMARY_GAME_MODULE(FFundamentGameModule, FundamentGame, "FundamentGame");
