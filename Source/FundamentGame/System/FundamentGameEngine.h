// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Engine/GameEngine.h"
#include "UObject/UObjectGlobals.h"

#include "FundamentGameEngine.generated.h"

class IEngineLoop;
class UObject;

/**
 * Engine that manages core systems that enable a game.
 * This class is used by setting 'GameEngine' and 'UnrealEdEngine' in DefaultEngine.ini.
 */
UCLASS()
class UFundamentGameEngine : public UGameEngine
{
	GENERATED_BODY()

public:

	UFundamentGameEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:

	virtual void Init(IEngineLoop* InEngineLoop) override;
};
