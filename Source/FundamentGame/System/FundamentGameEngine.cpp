// Copyright Epic Games, Inc. All Rights Reserved.

#include "FundamentGameEngine.h"

class IEngineLoop;

UFundamentGameEngine::UFundamentGameEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UFundamentGameEngine::Init(IEngineLoop* InEngineLoop)
{
	Super::Init(InEngineLoop);
}

