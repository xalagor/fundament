// Copyright Epic Games, Inc. All Rights Reserved.

#include "FundamentEditorEngine.h"
#include "ISourceControlModule.h"
#include "ISourceControlProvider.h"

class IEngineLoop;

#define LOCTEXT_NAMESPACE "FundamentEditor"

UFundamentEditorEngine::UFundamentEditorEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UFundamentEditorEngine::Init(IEngineLoop* InEngineLoop)
{
	Super::Init(InEngineLoop);

	// Register state branches
	const ISourceControlModule& SourceControlModule = ISourceControlModule::Get();
	{
		ISourceControlProvider& SourceControlProvider = SourceControlModule.GetProvider();
		// Order matters. Lower values are lower in the hierarchy, i.e., changes from higher branches get automatically merged down.
		// (Automatic merging requires an appropriately configured CI pipeline)
		// With this paradigm, the higher the branch is, the stabler it is, and has changes manually promoted up.
		const TArray<FString> Branches{ "origin/release", "origin/stable", "origin/promoted", "origin/trunk", "origin/main" };
		SourceControlProvider.RegisterStateBranches(Branches, TEXT("Content"));
	}
}

void UFundamentEditorEngine::Start()
{
	Super::Start();
}

void UFundamentEditorEngine::Tick(float DeltaSeconds, bool bIdleMode)
{
	Super::Tick(DeltaSeconds, bIdleMode);
}

#undef LOCTEXT_NAMESPACE
