// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Editor/UnrealEdEngine.h"
#include "FundamentEditorEngine.generated.h"

class IEngineLoop;

UCLASS()
class UFundamentEditorEngine : public UUnrealEdEngine
{
	GENERATED_BODY()

public:

	UFundamentEditorEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:

	virtual void Init(IEngineLoop* InEngineLoop) override;
	virtual void Start() override;
	virtual void Tick(float DeltaSeconds, bool bIdleMode) override;
};