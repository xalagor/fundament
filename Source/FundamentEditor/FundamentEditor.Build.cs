// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FundamentEditor : ModuleRules
{
    public FundamentEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[] { "FundamentEditor" });

        PrivateIncludePaths.AddRange(new string[] {});

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "UnrealEd",
            "FundamentGame",
        });

        PrivateDependencyModuleNames.AddRange(new string[] {
            "InputCore",
            "Slate",
            "SlateCore",
            "SourceControl",
        });

        DynamicallyLoadedModuleNames.AddRange(new string[] {});
    }
}
