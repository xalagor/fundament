// Copyright Epic Games, Inc. All Rights Reserved.

#include "FundamentEditor.h"

#define LOCTEXT_NAMESPACE "FundamentEditor"

DEFINE_LOG_CATEGORY(LogFundamentEditor);

class FFundamentEditorModule : public FDefaultGameModuleImpl
{
    typedef FFundamentEditorModule ThisClass;
};

IMPLEMENT_MODULE(FFundamentEditorModule, FundamentEditor);

#undef LOCTEXT_NAMESPACE