// Copyright Denis Xalagor Petrov. MIT Licensed.

using System.Collections.Generic;
using UnrealBuildTool;

public class FundamentGameTarget : TargetRules
{
    public FundamentGameTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.AddRange(new string[] { "FundamentGame" });
    }
}
